#!/usr/bin/env ruby

require 'docopt'
require 'erb'
require 'httparty'
require 'yaml'

docstring = <<DOCSTRING
Create a backend retrospective issue

Usage:
  #{__FILE__} --token=<token> --team=<team> [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help        Show this screen.
  --dry-run        Print the issue contents to standard output.
  --token=<token>  GitLab API token.
  --team=<team>    Team name, including initial caps (team info in teams.yml).
DOCSTRING

ENDPOINT = 'https://gitlab.com/api/v4'

def gitlab_get(path, token: nil)
  path = "#{ENDPOINT}/#{path}" unless path.start_with?(ENDPOINT)

  options = {}
  options[:headers] = { 'Private-Token': token } if token

  HTTParty.get("#{path}&per_page=50", options)
end

def gitlab_post(path, body:, token:)
  path = "#{ENDPOINT}/#{path}" unless path.start_with?(ENDPOINT)

  options = {
    body: body,
    headers: { 'Private-Token': token }
  }

  response = HTTParty.post(path, options)

  unless response.code == 201
    puts "POST to #{path} failed! Request body:"
    p body
    puts ''
    puts "Response code: #{response.code}. Body:"
    puts response.body

    exit 1
  end

  response
end

def log_info(header, message, dry_run:)
  puts '-' * 70
  puts "#{header} to create#{' (--dry-run enabled)' if dry_run}"
  puts '-' * 70
  puts message
  puts '-' * 70
  puts ''
end

def create_issue(token:, team:, dry_run:)
  team_info = YAML.load_file('teams.yml')[team]
  due_date = Time.now.strftime('%Y-%m-22')
  issue_url = nil

  # We can't pick the last milestone, because it will be the one in current
  # development, assuming that this is run after the freeze.
  today = Time.now.strftime('%Y-%m-%d')
  release = gitlab_get('groups/gitlab-org/milestones?state=active', token: token)
              .select { |milestone| milestone['start_date'] && milestone['start_date'] < today }
              .sort_by { |x| x['start_date'] }[-2]['title']

  deliverables = gitlab_get("groups/gitlab-org/issues?labels=#{team_info['label']},Deliverable&state=closed&milestone=#{release}")
  slipped = gitlab_get("groups/gitlab-org/issues?labels=#{team_info['label']},missed%3A#{release}")
  follow_up = gitlab_get("projects/gl-retrospectives%2F#{team_info['project']}/issues?labels=follow-up&state=opened")

  template = ERB.new(File.read('template.erb'), nil, '<>')
  result = template.result(binding)

  log_info('Issue', result, dry_run: dry_run)

  unless dry_run
    created_issue = gitlab_post(
      "projects/gl-retrospectives%2F#{team_info['project']}/issues",
      body: {
        title: "#{release} #{team} retrospective",
        description: result,
        confidential: true
      },
      token: token
    )

    issue_url = created_issue['_links']['self']

    puts "Created with URL #{issue_url}"
    puts ''
  end

  mention_team(issue_url, token: token, team_info: team_info, dry_run: dry_run)
  create_discussions(issue_url, token: token, dry_run: dry_run)
end

def mention_team(issue_url, token:, team_info:, dry_run:)
  comment = ['backend', 'frontend', 'ux', 'pm'].map do |people|
    [
      '*',
      *team_info[people].map { |person| "@#{person}" },
      "(#{people})"
    ].join(' ')
  end.join("\n")

  log_info('Mention comment', comment, dry_run: dry_run)

  unless dry_run
    gitlab_post("#{issue_url}/notes", body: { body: comment }, token: token)

    puts 'Created comment'
    puts ''
  end
end

def create_discussions(issue_url, token:, dry_run:)
  questions = [
    'What went well this release?',
    'What didn\'t go well this release?',
    'What can we improve?'
  ]

  questions.each do |question|
    comment = "## #{question}"

    log_info('Discussion', comment, dry_run: dry_run)

    unless dry_run
      gitlab_post("#{issue_url}/discussions", body: { body: comment }, token: token)
    end
  end

  puts 'Created discussions'
  puts ''
end

begin
  options = Docopt::docopt(docstring)

  token = options.fetch('--token')
  team = options.fetch('--team')
  dry_run = options.fetch('--dry-run', false)

  create_issue(token: token, team: team, dry_run: dry_run)
rescue Docopt::Exit => e
  puts e.message
end

# Backend retrospective issue creator

Create an issue for use in a [team retrospective] automatically.

This project has a [pipeline schedule] to create retrospectives on the 9th of
each month (after the missed deliverable labels have been added).

To add a new team, add a job to [`.gitlab-ci.yml`](.gitlab-ci.yml) and a team to
[`teams.yml`](teams.yml).

## Local testing

```shell
$ bundle
$ bundle exec ruby retrospective.rb
```

For example, this will write the issue description for a Plan retrospective to
standard output. (To actually create an issue, remove `--dry-run`.)

```shell
$ bundle exec ruby retrospective.rb --dry-run --token="$GITLAB_API_TOKEN" --team=Plan
```

[team retrospective]: https://about.gitlab.com/handbook/engineering/management/team-retrospectives/
[pipeline schedule]: https://gitlab.com/gitlab-org/backend-retrospectives/pipeline_schedules
